<?php echo form_open('products/edit'); ?>
<?php echo form_hidden('product_id', $data_produk[0]->product_id); ?>
<?php echo $this->session->flashdata('edit'); ?>
<h1>EDIT PRODUK</h1>
<table>

    <tr><td>Product ID</td><td><?php echo form_input('', $data_produk[0]->id, "disabled"); ?></td></tr>

    <tr><td>category_id</td><td><?php echo form_input('category_id', $data_produk[0]['category_id']); ?></td></tr>

    <tr><td>product_name</td><td><?php echo form_input('product_name', $data_produk[0]['product_name']); ?></td></tr>

    <tr><td>product_brand</td><td><?php echo form_input('product_brand', $data_produk[0]['product_brand']); ?></td></tr>

    <tr><td>produk_info</td><td><?php echo form_input('product_info', $data_produk[0]['product_info']); ?></td></tr>

    <tr><td colspan="2">
            <?php echo form_submit('submit', 'Simpan'); ?>
            <?php echo anchor('products', 'Kembali'); ?></td></tr>

</table>
<?php
echo form_close();
?>