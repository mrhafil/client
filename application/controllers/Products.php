<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	var $api ="";
    function __construct() {
        parent::__construct();
        $this->api="http://localhost/belajarLuman/public/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
		$this->load->helper('url');
	}

	public function index()
	{
		$content = file_get_contents("http://localhost/belajarLumen/public/products");

		$content = utf8_encode($content);

		$hasil = json_decode($content,true);

		$data = array (
			'list_products' => $hasil['results'],
		);

        $this->load->view('products/list',$data);
	}

	public function tambah() {
		if (isset($_POST['tambah'])) {
            $data = array(
                'category_id' => $this->input->post('category_id'),
                'product_name' => $this->input->post('product_name'),
                'product_brand' => $this->input->post('product_brand'),
                'product_info' => $this->input->post('product_info')
            );
            $insert = $this->curl->simple_post($this->api . '/insert', $data, array(CURLOPT_BUFFERSIZE => 10));
            if ($insert) {
                $this->session->set_flashdata('info', 'data berhasil disimpan.');
            } else {
                $this->session->set_flashdata('info', 'data gagal disimpan.');
            }
            redirect('products');
        } else {
            $this->load->view('products/v_form');
        }
    }
    public function edit() {
        if (isset($_POST['submit'])) {
            $data = array(
                'product_id' => $this->input->get('product_id'),
                'category_id' => $this->input->post('category_id'),
                'product_name' => $this->input->post('product_name'),
                'product_brand' => $this->input->post('product_brand'),
                'product_info' => $this->input->post('product_info'),
            );
                // 'stok' => $this->input->post('stok'));
            $update = $this->curl->simple_put($this->api . '/update', $data, array(CURLOPT_BUFFERSIZE => 10));
            if ($update) {
                $this->session->set_flashdata('edit', 'Data Berhasil diubah');
            } else {
                $this->session->set_flashdata('edit', 'Data Gagal diubah');
            }
            redirect('products');
        } else {
            $params = array('product_id' => $this->uri->segment(3));
            $data['data_produk'] = json_decode($this->curl->simple_get($this->api . '/products', $params));
            $this->load->view('products/v_edit', $data);
        }
    }

    public function delete($product_id) {
        if (empty($product_id)) {
            redirect('products');
        } else {
            $delete = $this->curl->simple_delete($this->api . '/delete/{product_id}', array('product_id' => $product_id), array(CURLOPT_BUFFERSIZE => 10));
            if ($delete) {
                $this->session->set_flashdata('info', 'Data Berhasil dihapus');
            } else {
                $this->session->set_flashdata('info', 'Data Gagal dihapus');
            }
            redirect('products');
        }
    }
}